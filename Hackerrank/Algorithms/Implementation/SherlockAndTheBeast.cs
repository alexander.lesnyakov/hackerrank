﻿using System;
using System.Text;
using Hackerrank.Extensions;

namespace Hackerrank.Algorithms.Implementation
{
    /// <summary>
    ///     Solution for https://www.hackerrank.com/challenges/sherlock-and-the-beast
    /// </summary>
    public static class SherlockAndTheBeast
    {
        private const string NoSolutionResult = "-1";

        public static string Solution(string size, string elements)
        {
            if (string.IsNullOrWhiteSpace(size))
                return NoSolutionResult;

            var t = Convert.ToInt32(size);
            if (t == 0)
                return NoSolutionResult;

            var items = elements.SplitByNewLine();
            var result = string.Empty;
            for (var i = 0; i < t; i++)
            {
                var n = Convert.ToInt32(items[i]);
                if (HasNoSolution(n))
                {
                    result += NoSolutionResult + Environment.NewLine;
                    continue;
                }

                result += GetSolution(n);
                if (i < t - 1)
                    result += Environment.NewLine;
            }

            return result;
        }

        private static string GetSolution(int number)
        {
            var numberLength = number;
            while (numberLength >= 3)
            {
                if (IsDividedByThree(numberLength))
                    return numberLength == number
                        ? GetStringOfDigits(numberLength, '5')
                        : GetStringOfDigits(numberLength, '5') + GetStringOfDigits(number - numberLength, '3');

                if (numberLength <= 10 && IsDividedByFive(numberLength))
                    return GetStringOfDigits(numberLength, '3');

                numberLength -= 5;
            }

            return "-1";
        }

        private static string GetStringOfDigits(int length, char digit)
        {
            return new StringBuilder(length).Append(digit, length).ToString();
        }

        private static bool HasNoSolution(int numberLength)
        {
            return numberLength == 1 || numberLength == 2 || numberLength == 4 || numberLength == 7;
        }

        private static bool IsDividedByThree(int number)
        {
            return number%3 == 0;
        }

        private static bool IsDividedByFive(int number)
        {
            return number%5 == 0;
        }
    }
}