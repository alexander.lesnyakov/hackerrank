﻿using System;
using Hackerrank.Extensions;

namespace Hackerrank.Algorithms.Warmup
{
    /// <summary>
    ///     Solution for https://www.hackerrank.com/challenges/diagonal-difference
    /// </summary>
    public static class DiagonalDifference
    {
        public static string Solution(string size, string elements)
        {
            if (string.IsNullOrWhiteSpace(size))
                return string.Empty;

            var n = Convert.ToInt32(size);
            if (n == 0)
                return string.Empty;

            var a = new int[n][];

            var rows = elements.SplitByNewLine();
            for (var aI = 0; aI < n; aI++)
            {
                var aTemp = rows[aI].Split(' ');
                a[aI] = Array.ConvertAll(aTemp, int.Parse);
            }

            var sumFirst = 0;
            var sumSecond = 0;

            var j = n - 1;
            for (var i = 0; i < n; i++)
            {
                sumFirst += a[i][i];
                sumSecond += a[j--][i];
            }

            return Math.Abs(sumFirst - sumSecond).ToString();
        }
    }
}