﻿using System;

namespace Hackerrank.Extensions
{
    public static class StringExtensions
    {
        internal static string[] SplitByNewLine(this string input)
        {
            return input.Split(new[] {"\r\n", "\n"}, StringSplitOptions.None);
        }
    }
}