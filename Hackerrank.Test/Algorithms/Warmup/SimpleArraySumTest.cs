﻿using Hackerrank.Algorithms.Warmup;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Hackerrank.Test.Algorithms.Warmup
{
    [TestClass]
    public class SimpleArraySumTest
    {
        [TestMethod]
        public void TestSolution()
        {
            const string arraySize = "6";
            const string elements = "1 2 3 4 10 11";
            Assert.AreEqual("31", SimpleArraySum.Solution(arraySize, elements));
        }

        [TestMethod]
        public void TestSolutionEmptyArray()
        {
            const string arraySize = "0";
            const string elements = "";
            Assert.AreEqual(string.Empty, SimpleArraySum.Solution(arraySize, elements));
        }

        [TestMethod]
        public void TestSolutionEmptyArraySize()
        {
            const string arraySize = "";
            const string elements = "";
            Assert.AreEqual(string.Empty, SimpleArraySum.Solution(arraySize, elements));
        }
    }
}