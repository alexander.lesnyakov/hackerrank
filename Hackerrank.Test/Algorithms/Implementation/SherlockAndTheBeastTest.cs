﻿using Hackerrank.Algorithms.Implementation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Hackerrank.Test.Algorithms.Implementation
{
    [TestClass]
    public class SherlockAndTheBeastTest
    {
        [TestMethod]
        public void TestSolution()
        {
            const string size = "4";
            const string elements =
                @"1
3
5
11";
            const string expectedResult =
                @"-1
555
33333
55555533333";
            Assert.AreEqual(expectedResult, SherlockAndTheBeast.Solution(size, elements));
        }

        [TestMethod]
        public void TestSolutionZeroArraySize()
        {
            const string size = "0";
            const string elements = "";
            Assert.AreEqual("-1", SherlockAndTheBeast.Solution(size, elements));
        }

        [TestMethod]
        public void TestSolutionEmptyArraySize()
        {
            const string size = "";
            const string elements = "";
            Assert.AreEqual("-1", SherlockAndTheBeast.Solution(size, elements));
        }
    }
}